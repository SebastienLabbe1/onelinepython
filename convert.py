#!/usr/bin/env python3
import sys
import re

allR = "[a-zA-Z0-9_ \t]*"
varR = "[a-zA-Z0-9_ ]*"

split = lambda exp : (lambda line : [x.strip() for x in line.split(exp)])

rules = [
        (allR + "=" + allR, split("="), "[{0} for {1} in [{2}]][0]")
        ("for " + varR " in " + varR, split
        ]

N = 10

def change(f, line):
    sline = f(line)
    while len(sline) < N:
        sline.append("")
    return sline

def printUsage():
    print("./convert <filename>")
    exit()

def throw():
    print("file does not have the right format")
    exit()

def convertText(text):
    lines = text.split("\n")
    ol = ""
    for i, line in reversed(list(enumerate(lines))):
        if line.strip() == "": continue
        print(i, line)
        if "def" in line: 
            ol = line + " return " + ol
            break
        if "return" in line:
            ol = line.split("return")[-1].strip()
            continue
        for exp, f, res in rules:
            if re.match(exp, line):
                sline = change(f, line)
                ol = res.format(ol, sline[0], sline[1])
                break
        else:
            throw()
    print(ol)
    return ol

def saveText(filename, text):
    with open(filename + "ol.py", "w") as file:
        file.write(text)

def readFile(filename):
    with open(filename + ".py") as file:
        return file.read()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        printUsage()
    filename = sys.argv[1]
    text = readFile(filename)
    text2 = convertText(text)
    saveText(filename, text2)


